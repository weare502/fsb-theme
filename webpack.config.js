const path = require('path');

module.exports = {
    entry: './static/js/site-vue.js',
    output: {
        path: path.resolve(__dirname, 'static/js/dist'),
        filename: 'main.js'
    }
};