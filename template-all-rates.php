<?php
/**
 * Template Name: All Rates
 *
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();
$context['post'] = $post;

$data_tables = Timber::get_posts(array( 'post_type' => 'data_table', 'posts_per_page' => -1, 'order' => 'ASC' ));

/**
 * We're about to format a big array in a similar way to the ACF Table field
 */

foreach ($data_tables as $key => $table){
    if ( ! $table->get_field('include_in_all_rates_table') ){
        unset($data_tables[$key]);
    }
}
// reindex array
$data_tables = array_values($data_tables);

$table = array(
    'header' => array(
        array( 'c' => 'Product' )
    ),
    'body' => array()
);

$i = 0;

// Get the table headings
foreach ($data_tables[0]->get_field('table')['body'] as $tr ){
    if ( $i > 4 ){
        // stop at 5 columns
        break;
    }

    // var_dump($tr[0]['c']);
    // only extract the first column
    $table['header'][] = $tr[0];

    $i++;
}

foreach ($data_tables as $dt){
    $i = 0;
    $t = $dt->get_field('table');
    $row_values = array(
        array( 'c' => $dt->title )
    );
    foreach ($t['body'] as $tr){
        if ( $i > 4 ){
            break;
        }
    
        // skip the first column
        $row_values[] = $tr[1];

        $i++;
    }
    $table['body'][] = $row_values;
    
}

// die( );


$context['table'] = $table;


Timber::render( array( 'all-rates.twig' ), $context );