<?php
/**
 * Template Name: Wide Page
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();
$context['post'] = $post;
$context['wide'] = true;

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );
}