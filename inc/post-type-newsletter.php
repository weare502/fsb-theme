<?php

$labels = array(
	'name'               => __( 'Newsletters', 'spha' ),
	'singular_name'      => __( 'Newsletter', 'spha' ),
	'add_new'            => _x( 'Add New Newsletter', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Newsletter', 'spha' ),
	'edit_item'          => __( 'Edit Newsletter', 'spha' ),
	'new_item'           => __( 'New Newsletter', 'spha' ),
	'view_item'          => __( 'View Newsletter', 'spha' ),
	'search_items'       => __( 'Search Newsletters', 'spha' ),
	'not_found'          => __( 'No Newsletters found', 'spha' ),
	'not_found_in_trash' => __( 'No Newsletters found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Newsletter:', 'spha' ),
	'menu_name'          => __( 'Newsletters', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-pressthis',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'newsletter', $args );