<?php

$labels = array(
	'name'               => __( 'Footer CTAs', 'spha' ),
	'singular_name'      => __( 'Footer CTA', 'spha' ),
	'add_new'            => _x( 'Add New Footer CTA', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Footer CTA', 'spha' ),
	'edit_item'          => __( 'Edit Footer CTA', 'spha' ),
	'new_item'           => __( 'New Footer CTA', 'spha' ),
	'view_item'          => __( 'View Footer CTA', 'spha' ),
	'search_items'       => __( 'Search Footer CTAs', 'spha' ),
	'not_found'          => __( 'No Footer CTAs found', 'spha' ),
	'not_found_in_trash' => __( 'No Footer CTAs found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Footer CTA:', 'spha' ),
	'menu_name'          => __( 'Footer CTAs', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-megaphone',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'footer_cta', $args );