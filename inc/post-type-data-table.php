<?php

$labels = array(
	'name'               => __( 'Data Tables', 'spha' ),
	'singular_name'      => __( 'Data Table', 'spha' ),
	'add_new'            => _x( 'Add New Data Table', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Data Table', 'spha' ),
	'edit_item'          => __( 'Edit Data Table', 'spha' ),
	'new_item'           => __( 'New Data Table', 'spha' ),
	'view_item'          => __( 'View Data Table', 'spha' ),
	'search_items'       => __( 'Search Data Tables', 'spha' ),
	'not_found'          => __( 'No Data Tables found', 'spha' ),
	'not_found_in_trash' => __( 'No Data Tables found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Data Table:', 'spha' ),
	'menu_name'          => __( 'Data Tables', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-editor-table',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'data_table', $args );