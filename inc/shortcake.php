<?php

add_shortcode('data-table', function($attr){
    $attr = wp_parse_args( $attr, array(
        'id' => false,
        'subtitle' => ''
    ) );

    if ( ! $attr['id'] ){
        return "<p style='background: red; color: white; padding: .5rem;'>No Data Table ID present in Shortcode</p>";
    }

    $context = Timber::get_context();
    $context['post'] = Timber::get_post( absint( $attr['id'] ) );
    $ret = Timber::compile( 'data-table-embed.twig', $context );
    
    return $ret;
} );

if ( ! function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
    return; // bail early if shortcake isn't installed
}

shortcode_ui_register_for_shortcode(
    'data-table', array(
        'label' => 'Data Table',
        'listItemImage' => 'dashicons-editor-table',
        'attrs' => array(
            array(
                'label' => 'Date Table to Embed',
                'attr' => 'id',
                'type' => 'post_select',
                'query' => array( 'post_type' => 'data_table' )
            )
        )
    )
);
