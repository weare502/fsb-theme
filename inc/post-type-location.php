<?php

$labels = array(
	'name'               => __( 'Locations', 'spha' ),
	'singular_name'      => __( 'Location', 'spha' ),
	'add_new'            => _x( 'Add New Location', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Location', 'spha' ),
	'edit_item'          => __( 'Edit Location', 'spha' ),
	'new_item'           => __( 'New Location', 'spha' ),
	'view_item'          => __( 'View Location', 'spha' ),
	'search_items'       => __( 'Search Locations', 'spha' ),
	'not_found'          => __( 'No Locations found', 'spha' ),
	'not_found_in_trash' => __( 'No Locations found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Location:', 'spha' ),
	'menu_name'          => __( 'Locations', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-store',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'location', $args );