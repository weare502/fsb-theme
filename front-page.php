<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );