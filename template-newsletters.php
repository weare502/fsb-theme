<?php
/**
 * Template Name: Newsletters
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['newsletters'] = Timber::get_posts( array( 'post_type' => 'newsletter', 'posts_per_page' => -1 ) );
$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();

Timber::render( 'newsletters.twig', $context );