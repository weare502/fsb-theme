/* global Vue */
/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */


var freezeVp = function(e) {
    e.preventDefault();
};

window.stopBodyScrolling = function(bool) {
    if (bool === true) {
        document.body.addEventListener("touchmove", freezeVp, false);
    } else {
        document.body.removeEventListener("touchmove", freezeVp, false);
    }
};


jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$body.fitVids();

	$('#menu-toggle').click(function(){
		if ( $('#nav-main').hasClass('menu-open') ){
			$('#nav-main').removeClass('menu-open');
			$('body').css('position', 'static');
		} else {
			$('#nav-main').addClass('menu-open');
			$('#nav-main').removeClass('login-open');
			$('body').css('position', 'fixed');
		}
	});

	$('#login-toggle').click(function(){
		if ( $('#nav-main').hasClass('login-open') ){
			$('#nav-main').removeClass('login-open');
			$('body').css('position', 'static');
		} else {
			$('#nav-main').addClass('login-open');
			$('#nav-main').removeClass('menu-open');
			$('body').css('position', 'fixed');
		}
	});

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	
	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true
	});

	$('#primary-menu a[href="#"]').each(function(){
		// $(this).parents('#primary-menu > li').attr('tabindex', 0);
		$(this).replaceWith("<span>" + $(this).html() + "</span>");
	});

	window.remoteLogin = new Vue({
		el: '#remote-login',
		data: {
			currentTab: 'personal'
		},
		watch: {
			currentTab: function(newval){
				this.$nextTick(function(){
					this.$refs[newval].focus();
				});
			}
		},
		methods: {
			sendEvent: function( type, e ){
				// Keeps track of whether or not the form has been submitted.
				// This prevents the form from being submitted twice in cases
				// where `hitCallback` fires normally.
				var formSubmitted = false;

				var submitForm = function() {
					if (!formSubmitted) {
						formSubmitted = true;
						e.target.submit();
					}
				};

				if ( typeof window.__gaTracker === "function" ){
					window.__gaTracker('send', 'event', type + ' Online Banking Login', 'submit', {
						hitCallback: submitForm
					});
				}
				setTimeout(submitForm, 1000);

			}
		}
	});

	if ( $('.page-template-template-contact').length ){
		window.contactTabs = new Vue({
			el: '.contact__wrapper',
			data: {
				currentTab: 'Westmoreland'
			}
		});
	}

	$('a:not([href*="' + window.location.hostname + '"]):not([href^="#"]):not([href^="/"]):not([href*="secureinternetbank.com"])').addClass('external');

	$('a.external').not('.outgoing').click( function( event ) {
        var url = $(this).attr('href');
        var message = '';
		message = "By clicking this link, you will leave the bank’s site. Farmers State Bank does not guarantee or endorse the products, security, or privacy practices on linked sites. Farmers State Bank is not responsible for the accuracy of data on linked sites.";
		message = window.fsbExternalLinkText;
        event.preventDefault();
        var confirm_box = confirm( message );
        if (confirm_box) {
            window.open( url );
        }
	});

	$('a[href*=".pdf"]:not(#footer a):not(.pattern-footer-cta a):not(.apps-grid__grid-item a):not(.newsletters__item a), .pattern-link-bar a[href*=".pdf"], a.pdf-application-notice, .pdf-application-notice > a').click( function(e) {
		var url = $(this).attr('href');
        var message = '';
		message = "PLEASE NOTE: Emailing completed applications is not secure and could put your information at risk. Please mail or drop off your application to your nearest branch, or contact a loan officer for instructions on how to submit your application securely online.";
		message = window.fsbPdfNoticeText;
        e.preventDefault();
        var confirm_box = confirm( message );
        if (confirm_box) {
            window.open( url );
        }
	});

	/**
	 * Commenting out to fix "bug". If the client can point to a specific spot where this is breaking more, we may add a note here to trigger the popup.
	 */
	// $('.pattern-footer-cta a:not([href*="contact-us"])').click(function(e) {
	// 	var url = $(this).attr('href');
    //     var message = '';
	// 	message = "PLEASE NOTE: Emailing completed applications is not secure and could put your information at risk. Please mail or drop off your application to your nearest branch, or contact a customer service representative for instructions on how to submit your application securely online.";
	// 	message = window.fsbDebitNoticeText;
    //     e.preventDefault();
    //     var confirm_box = confirm( message );
    //     if (confirm_box) {
    //         window.open( url );
    //     }
	// });

	// Forms page anchors only
	$('.apps-grid__grid-item a').click(function(e) {
		var url = $(this).attr('href');
        var message = '';
		message = "PLEASE NOTE: Emailing completed applications is not secure and could put your information at risk. Please mail or drop off your application to your nearest branch, or contact a loan officer or customer service representative for instructions on how to submit your application securely online.";
		message = window.fsbFormsNoticeText;
        e.preventDefault();
        var confirm_box = confirm( message );
        if (confirm_box) {
            window.open( url );
        }
	});

	// Get the height of the content area for each one, and then hide it. (prevents the "jump" from slideToggle() not getting the right height)
	$('.accordion-content').each(function() {
		$height = $(this).height();
		$(this).css('height', $height);
		$(this).hide();
	});

	// dynamically open and close the faq boxes / rotate the chevron
	$('.accordion-label').click( function() {
		$(this).toggleClass('acc-toggled');
		$(this).next('.accordion-content').slideToggle(700);
	});

}); // End Document Ready