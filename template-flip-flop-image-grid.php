<?php
/**
 * Template Name: Flip Flop Image Grid
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();

Timber::render( 'flip-flop-image-grid.twig', $context );